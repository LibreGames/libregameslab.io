.. title: Libre Games
.. slug: index
.. date: 2017-05-19 18:52:46 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

What is Libre Games?
--------------------

Libre Games is a collective of developers of free/libre and open source
games, brought together by their wish to take over the maintainership
of abandoned libre games.

The focus is therefore not on developing brand new projects together,
but on giving a common home to existing loosely maintained games.

The idea is that any motivated contributor can join the Libre Games
collective and help maintain one or more of the supported projects, or
even bring a new interesting but inactive project.

Our games
---------

The current projects maintained by Libre Games are:

- `Jump 'n Bump </jumpnbump/>`_, a cute multiplayer platform game with
  bunnies
- `Open Alchemist </openalchemist/>`_, a reflexion game similar to
  Columns (*coming soon*)
- `FreeTumble </freetumble/>`_, a colorful tile-matching puzzle game
- `JAG </jag/>`_, a match-3 arcade puzzle game

See also the `Lugaru <https://osslugaru.gitlab.io>`_ game which is
developed outside the Libre Games organization but by several of its
members.

Source code
-----------

All our projects are free and open source. Their respective source
repositories can be found on `GitLab <https://gitlab.com/LibreGames>`_.
