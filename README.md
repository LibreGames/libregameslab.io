# Libre Games website

[![build status](https://gitlab.com/LibreGames/libregames.gitlab.io/badges/master/build.svg)](https://gitlab.com/LibreGames/libregames.gitlab.io/commits/master)

This repository holds the source code for the project website on https://libregames.gitlab.io

# Building the website

The website is built using [Nikola](https://getnikola.com/), and the source pages are written in [reStructuredText](http://docutils.sourceforge.net/rst.html).

Building the website locally is an easy process:

1. Check out the code
2. Install Nikola
3. Run `nikola build` in the root of the project

And if you want to see it locally, you can use `nikola serve --browser` to have it pop it open in your default web browser for you.

# Licensing

This site is licensed [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
